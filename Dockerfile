# Imagen base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copiado de archivos
ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependencias
RUN npm install

#Puerto que se expone
EXPOSE 3000

#Comando para ejecutar node
CMD ["npm", "start"]
